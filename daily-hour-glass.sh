#!/bin/bash

function get_num_participants() {
	read -p "Number of particpants: " num_participants
	echo $num_participants
}

function get_min_remaining() {
	read -p "Minutes remaining: " min_remaining
	echo $min_remaining
}

# paramters: <num_participants>  <min_remaining>
function get_secs_per_participant() {
	echo $(($2 * 60 / $1 ))
}

# parameters: <secs_per_participant>
function countdown() {
	VISIBLE_COUNTDOWN_LIMIT=10
	for (( i=$(($1 -1)); i>=0; i-- )); do
		sleep 1
		if [[ $i -le $VISIBLE_COUNTDOWN_LIMIT ]]; then
			echo "    $i"
		fi
	done
}

# parameters: <num_participants> <secs_per_participant>
function countdown_per_participant() {
	for PARTICIPANT in $(seq 1 $1); do
		countdown $2
		echo "----------------------->  YOUR TIME IS OVER <-----------------------"
	done
}

echo "Welcome to the daily."

NUM_PARTICIPANTS=$(get_num_participants)
MIN_REMAINING=$(get_min_remaining)
SECS_PER_PARTICIPANT=$(get_secs_per_participant $NUM_PARTICIPANTS $MIN_REMAINING)

echo "Time per person: $SECS_PER_PARTICIPANT seconds"
echo "Let's go!"

# start countdown per participant
countdown_per_participant $NUM_PARTICIPANTS $SECS_PER_PARTICIPANT
echo ""
echo "~\`\`-_~\`\`-_~\`\`-_~\`\`-_~\`\`-_~\`\`-~> THE END <~-\`\`-_~\`\`-_~\`\`-_~\`\`-_~\`\`-_~\`\`-_~"
echo ""
echo ""

